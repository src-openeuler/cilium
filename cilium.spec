%global debug_package %{nil}
%global goipath github.com/cilium/cilium

Name:           cilium
Version:        1.14.12
Release:        1
Summary:        eBPF-based Networking, Security, and Observability
License:        Apache-2.0 and OFL-1.1 and GPL-2.0-or-later and MPL-2.0 and MIT and CC-BY-SA-4.0 and ISC
URL:            https://github.com/cilium/cilium
Source0:        https://github.com/cilium/cilium/archive/refs/tags/%{version}.tar.gz

BuildRequires:  golang glibc-devel cmake systemd
BuildRequires:  gcc-c++ elfutils-libelf-devel libstdc++-static
BuildRequires:  libtool wget clang llvm go-bindata

Requires:       docker-engine >= 1.12 glibc-devel iproute >= 4.10 clang

%description
Cilium provides fast in-kernel networking and security policy enforcement
for containers based on eBPF programs generated on the fly. It is an
experimental project aiming at enabling emerging kernel technologies such
as BPF and XDP for containers.


%prep
%autosetup -n %{name}-%{version} -p1

%build
make build


%install
install -d -p %{buildroot}%{_bindir}
install -m 755 cilium/cilium %{buildroot}%{_bindir}/cilium
install -m 755 daemon/cilium-agent %{buildroot}%{_bindir}/cilium-agent
install -m 755 bugtool/cilium-bugtool %{buildroot}%{_bindir}/cilium-bugtool
install -m 755 operator/cilium-operator %{buildroot}%{_bindir}/cilium-operator
install -m 755 cilium-health/cilium-health %{buildroot}%{_bindir}/cilium-health
install -m 755 cilium-health/responder/cilium-health-responder %{buildroot}%{_bindir}/cilium-health-responder

install -d -p %{buildroot}%{_libdir}/systemd/system
install -m 755 contrib/systemd/cilium-consul.service %{buildroot}%{_libdir}/systemd/system/cilium-consul.service
install -m 755 contrib/systemd/cilium-etcd.service %{buildroot}%{_libdir}/systemd/system/cilium-etcd.service
install -m 755 contrib/systemd/cilium.service %{buildroot}%{_libdir}/systemd/system/cilium.service


%files
%{_bindir}/*
%{_libdir}/*

%changelog
* Fri Jun 14 2024 guoshengsheng <guoshengsheng@kylinos.cn> - 1.14.12-1
- update to 1.14.12
- fix CVE-2024-37307

* Sun Apr 07 2024 sunhai <sunhai10@huawei.com> - 1.14.9-1
- update to 1.14.9

* Thu Mar 21 2024 sunhai <sunhai10@huawei.com> - 1.14.8-1
- update to 1.14.8

* Sat Mar 09 2024 sunhai <sunhai10@huawei.com> - 1.14.7-1
- update to 1.14.7

* Thu Sep 28 2023 sunhai <sunhai10@huawei.com> - 1.14.2-1
- update to 1.14.2

* Fri May 5 2023 wangyongcong <m202071390@hust.edu.cn> - 1.11.16-1
- update to 1.11.16

* Fri Apr 14 2023 zhouwenpei <zhpouwenpei1@h-partners> - 1.11.15-1
- update to 1.11.15

* Tue May 10 2022 liukuo <liukuo@kylinos.cn> - 1.7.0-2
- Change the license to "Apache-2.0"

* Fri Jul 10 2020 openEuler Buildteam <buildteam@openeuler.org> - v1.7.0-1
- Package init
